pipeline {
    agent {
        node {
            label 'drone'
        }
    }

    parameters {
        string(
            name: 'GIT_REPO',
            defaultValue: '',
            description: 'Repository containing terraform configuration'
        )
        string(
            name: '_SLACK_CHANNEL',
            defaultValue: 'jenkins-pipeline-pve-e2e-deployment-test',
            description: 'Slack channel to receive notifications'
        )
    }

    stages {
        stage("CALL: provisioning/pipeline-pve-vm-create") {
            steps{
                script {
                    build job: 'provisioning/pipeline-pve-vm-create', 
                        parameters: [
                            string(name: 'GIT_REPO', value: params.GIT_REPO),
                            booleanParam(name: 'IPA_REGISTER',value: true),
                            booleanParam(name: 'IPA_GETAPPCERT',value: true),
                        ],
                        propagate: true,
                        wait: true
                }
            }
        }


        stage("CALL: provisioning/pipeline-pve-vm-destroy") {
            steps{
                script {
                    build job: 'provisioning/pipeline-pve-vm-destroy', 
                        parameters: [
                            string(name: 'GIT_REPO', value: params.GIT_REPO),
                        ],
                        propagate: true,
                        wait: true
                }
            }
        }
    }
    post {
        always {
            script {
                BUILD_USER = getBuildUser()
                COLOR_MAP = [
                'SUCCESS': 'good',
                'FAILURE': 'danger',
                ]
            }
            slackSend channel: params._SLACK_CHANNEL,
                color: COLOR_MAP[currentBuild.currentResult],
                message: """
                *${currentBuild.currentResult}:* Job ${env.JOB_NAME} build ${env.BUILD_NUMBER}
                GIT_URL: ${env.GIT_URL}
                GIT_BRANCH: ${env.GIT_BRANCH}
                GIT_COMMIT: ${env.GIT_COMMIT}
                More info at: ${env.BUILD_URL}
                """
        }
    
        cleanup {
            cleanWs()
        }
    }
}

String getBuildUser() {
    ID = currentBuild.getBuildCauses('hudson.model.Cause$UserIdCause').userId
    if (ID) {
      return ID
    }
    else {
      return '[cronTrigger: */5 * * * *]'
    }
}